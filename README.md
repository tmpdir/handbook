# The TMPDIR Handbook

Read online at [https://handbook.tmpdir.org](https://handbook.tmpdir.org)

## Why should I participate in a project like this?

There are two ways of looking at sharing what you know:

1. People will take this information and use it to compete against me. This is
   the scarcity mindset where we look at the world as a fixed pie getting more
   for me means others get less.
1. Sharing information will enable me to collaborate with the right people,
   attract more/better customers, and in the end accomplish more. It seems to be
   [working](https://changelog.com/podcast/397) for Gitlab. This is the
   abundance mindset where we view the "pie" of opportunity as constantly
   growing. There are more than enough problems in this world to solve -- there
   is no reason we have to fight over them.

## Editing

This document is composed of:

1. Markdown documents
2. Diagrams (mostly created with Draw.io)

[Typora](https://typora.io/) is a tool for editing markdown that provides a nice
experience. It is recommended you select the following options in the Typora
File->Preferences when editing:

![Typora Config](typora-config.png)

## Formatting

This Markdown source for this document is formatted using
[Prettier](https://prettier.io/) using a custom [prettier config](.prettierrc)
that wraps the width to 80 characters. There are trade-offs with wrapping vs not
wrapping. Diffs can be harder to review with wrapping. However, reading the
markdown in a text editor is a much nicer experience if the text is wrapped, so
we feel we should optimize for reading and editing vs reviewing. Please run the
`make`  before submitting any PRs to ensure the markdown files are all formatted
consistently.

## Generating Static Site

The static site is generated with [Zola](https://www.getzola.org/).

## Generating Output (obsolete)

[Pandoc](https://pandoc.org/) is used to generate `pdf` output. Again, simply
run the `make` command. Eventually, we'd like to generate HTML output and
include in the tmpdir.org web site.
