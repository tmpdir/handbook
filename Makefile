
date := $(shell date --rfc-3339=date)

all: format

format:
	find -name "*.md" -not -path "./themes/*" | xargs prettier --write

build:
	mdbook build

# the following no longer works since we switched to Zola/mdbook book for deployment
pandoc:
	mkdir -p output
	pandoc --number-sections --toc --write=pdf \
		-V colorlinks \
		-V geometry:margin=3cm \
		-V mainfont="DejaVu Serif" \
		-V monofont="DejaVu Sans Mono" \
		--pdf-engine=xelatex \
		-o output/tmpdir-handbook.pdf \
		0*.md
