# Operations

**Contents**

<!-- toc -->

(also see [Security](security.md) and [Reliability](reliability.md))

We'll define operations as operating the digital infrastructure we depend on.
For most companies operations is overhead and does not directly add to the
bottom line. Thus, operations are means to an end, and not an end in themselves.
Many IT personnel completely miss this point.

## Freedom

Freedom, as defined by Richard Hipp (the author of SQLite), means
[taking care of yourself](https://community.tmpdir.org/t/richard-hipp-sqlite-etc/389?u=cbrake).
In an operations sense, this means running stuff yourself where it makes sense.
The alternative is to lock yourself into a highly proprietary solution from one
provider that you can ever move away from. It is often more economical as well.
A few examples:

- with one $5/mo server (Linode, Digital Ocean, etc.), you can host dozens of
  services and sites. Though more convenient, it is much more expensive to
  deploy these services individually on App Engine, Beanstalk, Heroku, etc.
- with one $5/mo Server, I can easily host email for dozens of people and
  domains using
  [Simple NixOS Mail Server](https://community.tmpdir.org/t/simple-nixos-mailserver/469).
  The alternative is to pay per user at various mail hosting services.

There are tradeoffs -- if you have dynamic workloads that need to rapidly scale
up/down or if you need the integrated groupware features with email, then the
above may not make sense.

## Selecting services

### Don't use pay-as-you-go

Unless you really need to dynamically scale large, don't use pay-as-you-go
(PAYG) services like AWS, Cloudflare, GCP, etc. unless you can set hard limits.
Otherwise, a simple DNS attack can cause you to
[rack up $100,000's in bills](https://kerkour.com/denial-of-wallet-attacks-the-new-ddos#/).
If you are running a small service, run your services on fixed-fee subscription
service instead (ex: Linode). It may not be as cost effective, but the
management overhead and risk is much less.

### Can you get support if you need it?

99.9% of the time, everything goes fine and you don't need support. However,
when you need it, it is critical you can get it -- like when you can't log into
your account or a server upgrade failed. If you are not a big user, it may be
difficult to get support from the larger providers -- taking days of endless
hassle. Instead, select a smaller provider who offers good phone support (ex:
Linode).

### Diversify

You probably don't want to run all your infrastructure through one company. This
may be the most cost effective, but has several risks:

- you'll likely get locked into proprietary solutions that are hard to move away
  from should your needs change
- if your account gets shut down or locked for some reason, you lose everything
  leading to days/weeks/months of lost productivity
