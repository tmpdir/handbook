# Documentation

**Contents**

<!-- toc -->

Sub topics:

- [Documentation Formats](documentation-formats.md)

## The Role of Documentation

Documentation plays an important role in development. There are several ways we
can approach documentation:

1. As a set-in-stone specification that is used to drive the implementation of
   the project. In this model, senior developers or architects write the
   specification, and any changes to the specification need to be made by the
   original authors of the document. By the end of the project, the
   documentation is usually out of date and may bear little resemblance to what
   is actually produced.
2. As an active, living document that changes as the design changes and is used
   as part of the design process. As requirements and conditions change, the
   document is used as part of the [thinking](https://youtu.be/-4Yp3j_jk8Q) and
   collaborative process. When new features are starting, the approach is
   thought through and briefly documented before implementing. As the
   development proceeds, additional notes and changes are made to the
   documentation to reflect the approach, why things were done a certain way,
   etc.
3. Something that is done after the fact. This documentation is often of poor
   quality because the developers are not interested in this task -- they want
   to move on to the next project, and technical writers often do not completely
   understand the technical aspects to accurately capture the knowlege that
   should be captured.

Realistically, most probably spends time in all of the above, and there is some
value in each, but the more time you can spend doing active, living
documentation, the better your documentation will be, and the better your
thinking will be.

## Lead With Documentation

What this means in practice is when we start working on anything that requires a
little up front thought, architecture, or planning, the first thing we do is
open up the documentation for the project and start writing. If
collaboration/discussion is required, we open a pull request to facilitate
discussion. However, the approach is still documented in the repo files, not in
the PR, Github issue, etc.

This is very different than how most people work, but it is enjoyable and
productive. It takes effort to switch as our tendency is to put too much
documentation in Github issues, emails, etc. Old habits die hard and change
takes time.

For this approach to be effective, documentation needs to be frictionless.
Markdown is one solution – preferably in the same repo as the design/source
files. Then it is easy to edit code and docs in the same editor.

Some of the benefits to this approach:

1. when you are done with a task, the documentation is already done. Doing
   documentation after the fact is a rather distasteful task. However, if
   documentation is done before the task, it is very engaging.
2. it helps you think. As Leslie Lamport says,
   [_To think, you have to write. If you’re thinking without writing, you only think you’re thinking._](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/07/leslie_lamport.pdf)
3. documentation does not become stale, outdated, and useless.

## Share Information in Documentation, not Email or Messaging platforms

When someone asks for some information or how to do something, it's easiest to
just replay to an email, send a message in slack, etc. However, a better
approach is often to write some documentation, then share a link to the
document. Then the information is easily re-usable the next time someone asks,
or even better, people can find it themselves. This is a personal habit that
will multiply and scale your efforts. There are several other benefits:

- you will put a little more effort into a document than an email reply, so the
  information will be higher quality.
- the information can be improved over time.

Many people enjoy the feeling of power that comes from others needing the
information they have in their head or private archive. It feels good to be
needed and pushes people toward being the gatekeepers of their domain. However,
a more important question is are we effective? Are we optimizing for the short
term (feels good) or the long term (sustainable results that scale)? Do we want
our work and what we discover along the way to trancend ourself, or is our only
goal to serve ourselves?

## Why Wikis don't work for development documention

In a successful wiki, like Wikipedia, the documentation is the end goal, so it
is the focus – so a wiki works well there. In development documentation, the
documentation is NOT the end goal, so it is not the focus so documentation in a
wiki tends to be easily abandoned.

This
[commentary](https://dataswamp.org/~solene/2022-08-18-why-openbsd-documentation-is-good.html)
on the OpenBSD documentation is interesting. A quote:

> ### No wiki §
>
> This is an important point in my opinion, all the OpenBSD documentation is
> stored in the sources trees, they must be committed by someone with a commit
> access. Wiki often have orphan pages, outdated information, duplicates pages
> with contrary content. While they can be rich and useful, their content often
> tend to rot if the community doesn't spend a huge time to maintain them.

Wikis are extremely easy to edit (very low friction), but this results in no
workflow. Workflow or process is essential for quality and collaboration.
Markdown in Git is almost as easy as a Wiki to maintain, plus it has the added
benefit of PR workflow. In some cases Markdown is actually easier for developers
as you can make documentation changes directly in your code editor while
editting code.

## Tips for successful documentation

- docs must live in repos as close to design/code files as possible so they can
  be maintained as part of your normal development process.
- documentation must be in a format that is easy to review, edit, and maintain
  (Markdown seems to be the pragmatic choice)
- documentation must be open, accessible, and easily found. Again, Markdown
  shines here because you can easily link from one document to the next. You are
  not limited by the strict hierarchy of a filesystem. It is also easily viewed
  in the Git management tool (Gitea, Github, Gitlab, etc) without opening the
  file in another application.
- you must develop a culture and workflow where:
  - knowledge, decisions, theory of operation, etc is captured in documentation,
    not lost in the black hole of email and meeting discussion.
  - documentation is integral with the development process
  - discussions center around documentation

This all is hard -- really, really hard. But, I'm convinced it is one of the
great differentiators.

Some things you should consider not doing when working in a development team:

- email a document. Instead check it into a repo and email a link.
- write long technical information in an email. This information will be lost,
  instead write it in a document, and then send a link.
- extensively discuss things in email if there are better mediums like PRs,
  issues, forums, etc. where the discussion is accessible to anyone who has
  access and throughout time.
