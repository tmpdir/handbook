# Software

## Language Selection

As in many aspects of life, you can't have everything -- you need to optimize
for something. One way to look at programming languages is what I'll call the
"programming triangle." Similar to the
[project management triangle](https://community.tmpdir.org/t/development-triangle/71)
-- the programming triangle is: **Fast, Reliable, Simple** -- pick two -- you
can't have all three.

- **Efficient**: speed and size
- **Simple**: language complexity, tooling, deployment, library/package
  availability, etc.
- **Reliable**: low run time exceptions, secure, easily decipherable stack
  traces, etc.

![programming language triangle](images/programming-language-triangle.png)

There is also the issue of optimizing for domains. Rust is fast when running on
a native machine, but much slower and larger than Elm in the front-end. Thus it
often makes sense to use different languages depending on the domain you are
working in. Sometimes the domain dictates the language -- such as embedded
microcontroller programming where often C/C++ is the only practical option. Some
languages optimize one attribute aggressively at the expense of the other two.
One example is Python -- it is very simple to use, but compared to other options
is relatively unreliable and inefficient. However, when working on one-off data
science problems, simplicity and ease of use is most important -- especially
when paired with fast libraries. **There is a huge difference between writing
a** **program written by one person to solve one problem on one computer at one
point** **in time (examples: data science calculations, excel macros, and
university class** **assignments), and writing a program that will be developed
by a team, deployed** **on many systems, and maintained over an extended period
of time.** In the first case (what I call one-off programs), it really does
matter what you use. However, if we want a program to **scale** with developers,
deployments, features, and time, then we need to think seriously about the
programming triangle -- what is important for project success.

For programs that need to **scale**, we should be wary of languages that don't
do any, or only one of these attributes well, such as Javascript on the backend.
C++ is another example -- it is neither reliable or simple, but its momentum
keeps it in high use, and sometimes the availability of libraries (such as Qt)
make it a good choice.

When we think of a language, we might think first about the
[programming paradigm](https://en.wikipedia.org/wiki/Programming_paradigm) –
procedural, object oriented, functional etc. There is much more to software
development than the paradigm which is the “look” of the language. The
programming paradigm can be important, but for programs that need to scale, what
really matters is how the paradigm maps into the above attributes.

See also:

- Richard Feldmans
  [presentation about Rust and Elm](https://youtu.be/ukVqQGbxM9A)
