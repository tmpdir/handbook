# Summary

[Introduction](README.md)

- [Effective](effective.md)
- [The Development Process](development-process.md)
- [Collaboration](collaboration.md)
  - [Git](git.md)
- [Community](community.md)
- [Documentation](documentation.md)
  - [Documentation Formats](documentation-formats.md)
  - [Markdown](markdown.md)
- [Platform](platform.md)
- [Hardware](hardware.md)
- [Software](software.md)
- [Ops](ops.md)
- [Security](security.md)
- [Reliability](reliability.md)
