# Collaboration

**Contents**

<!-- toc -->

## Why is collaboration and process important?

Why is collaboration in a company, project, or organization important? There are
many reasons:

1. As systems increase in complexity, no one person knows everything about all
   the technology in a product, or has time to implement it all. Collaboration
   with experts is the only way to build complex systems.
2. Just as in product development where no one person has all the knowledge
   needed, how much more in the complex operations of a company composed of
   humans. A company's success depends on its ability to leverage good ideas of
   all involved, and there needs to be a process for these ideas to surface.
3. If design specifications are collaborative, vs authoritative, then they
   become a way for a team to think about the design. As
   [Leslie Lamport](http://lamport.org/) advocates,
   [thinking above the code](https://youtu.be/-4Yp3j_jk8Q) is important.
4. Although it is important to think about a design up front, complex systems
   cannot be completely specified up front. They evolve based on what we learn
   during implementation, feedback from users, and changing requirements.
   Development is an iterative process. If specifications and documentation are
   not an integral part of the working development process, they quickly become
   out of date an irrelevant. If specifications are maintained by one person on
   the team, they are likewise destined to become outdated and irrelevant.
   Everyone on the team needs a low friction way to edit and update
   documentation. The best case is if documentation becomes a useful tool during
   development -- a way to think, communicate, and collaborate.
5. A company is a collection of people. If people have a voice, they will be
   much more motivated than those who are simply told what to do. The command
   and control model of yesteryear does not work today.

## Openness and Transparency

People and organizations vary widely in the degree to which they are transparent
and open. Transparency and Openness are not exactly the same thing --
transparency is the degree to which we can see things. Openness is the quality
of being open to ideas and feedback. Both are necessary in a successful team.
Transparency is more about the process, and openness is more about the culture
-- both work together.

Share your work **early and often**. With Git hosting platforms like Github,
this is very easy to do. You simply push your working branch to a remote repo at
regular intervals -- at least daily while doing active work. This has many
benefits:

- Others can optionally look at what you are doing and can provide feedback.
  Note, optionally is the key word -- they don't have to look or respond like
  they might feel compelled to if you directly email them.
- Early feedback is valuable as others may have feedback that impacts the
  direction your implementation takes -- especially related to the business
  needs of the work.
- Sharing your work injects energy into the team. It is motivating to others to
  see things getting done. It is like the difference between a fast flowing
  mountain stream and a stagnant pond -- which is more inspiring?
- Small changes with good commit messages tells a nice story about a piece of
  work and is easy to review. Others can follow and understand what is doing
  without you having to explain as much. These stories are valuable later if
  anyone (usually ourselves) needs to go back and review why we did something
  the way we did.
- Monster commits/PRs are exhausting to review and end up not being reviewed.
- Commit messages are a form of writing and writing helps us think better.
- We do better work in public. Part of this may be accountability but part is
  simply how we work -- it is motivating.

Possible objections and counter responses:

- My code is not perfect.
  - _(Most people care more about things getting done than perfection. We all
    make mistakes and write sloppy code to prototype or experiment with things.
    That is perfectly acceptable.)_
- I don't want to bother people.
  - _(Git commit messages are easy to ignore. However, if the commit comments
    are good, someone who is casually observing the stream of work going by may
    see something that interests them and then take a closer look.)_
- It is only an experiment and may get redone anyway.
  - _(It is perfectly fine to do a git force push and rebase your changes. This
    is why these Git features exist. The Git hosting platform will still record
    the history in the pull request, which is instructive in showing others how
    you reached your solution, and it helps you as well better understand what
    you did.)_
- I'm not comfortable showing my work.
  - _(There may be many reasons for this. Some people are simply more private.
    Some feel insecure. Others have had bad experiences in the past where their
    work was criticized. As developers, we need to have thick skin -- not in the
    sense that we don't care, but that we are need easily offended. Many who
    share feedback are not trying to be harsh or critical, but simply have
    opinions on how things should be done -- we need to get used to this. As
    with anything, the easiest way to improve is to practice. If we share early
    and often, we'll learn how to deal with feedback.)_
- I don't have time for this.
  - _(If you are working on short term solo project, then this may be a valid
    argument. Otherwise, see
    [long term vs short term mindset](#the-long-term-vs-short-term-mindset).)_

Teams should have a cultural expectation that if you are doing work, there
should be commit messages, issue comments, something flowing in the stream of
changes. This may be documentation, notes, test code, todo lists, test results,
whatever -- just something. Even if we are only sketching in our Moleskine
notebook, we can still snap a photograph and share that.

## The long term vs short term mindset

Team work and collaboration is all about doing things in a slightly less
convenient way personally to make our work more transparent and helpful for the
team long term. Discussions in a Kanban board or Git pull request are less
convenient then sending a quick email. Creating a Markdown document in Git is
less convenient than firing up Word and emailing an attachment. Creating
granular Kanban cards for every task you do on a project is less convenient that
just diving in and doing the work. Using open source tools that are available to
anyone on the team may be less convenient than using a commercial tool we are
used to, but others do not have access to. Storing files in Git is less
convenient that a file server. Doing working on Git branches and opening pull
requests is less convenient than just pushing to master. Short term, this all
seems like overhead and more work. However, long term there are huge dividends.
Discussion happen in the context of work being done and are easy to find later.
The drudgery of status reports are eliminated because workflow is transparent
and meetings transition to creative discussions, brainstorming, and solving hard
problems. Conversations are centered around documentation rather than
documentation being created as an afterthought and often forgotten. Individuals
are no longer a bottleneck to work getting done. Changes are grouped in logical
pull requests that are easy to review when merged and referenced in the future.
Reviews allow us all to learn. Everyone has the access to tools to review and
fix anything any time any where. Long term maintenance is greatly simplified. We
understand what we are building. We see who is doing what. We understand why
things are done they way they are. The history of what we have done can be
leveraged for future decisions. We can visualize the flow of work. Above all, we
have clarity into what we are doing and why. Working with clarity brings many
intangible benefits that are not easily measured. Optimizing for the long term
always requires some personal sacrifice short term, but good tools and
methodologies minimize this personal sacrifice and often accrue personal
benefits many times over any investment we may have made. As an example, once
people learn Git (and there is a learning curve), they use it for all projects
-- even when a team is not involved as it is a better way to work. Good workflow
and tools takes the drudgery out of many repetitive tasks, reduce errors,
increase transparency, and free us to do creative work that adds value.

## The Role of Email

Email is an amazing communication tool. It is one of the marvels of the Internet
age and along with HTTP, perhaps one of the few truly distributed information
tools available that no one company controls. The distributed nature of email
(compared to silos like Linked-in, Twitter, Slack, etc) is vastly undervalued in
this age of large companies centralizing and monopolizing communication.
However, the negative aspect of email is that anyone can send you email and most
legitimate users tend to use it too much. Thus many people don't get much done
other than process email. We also tend to put too much information in emails.
Email is a very poor knowledge repository for the following reasons:

- it is not an accessible shared resource, especially for new people. Have you
  ever tried to search someone else's inbox?
- it is difficult to organize. You can have folders and tags, but you can't
  easily link from one email to another, which is a fundamental requirement of a
  good information system and the superpower of the Web.
- content cannot be updated or improved over time -- it can only be replied to.

As
[one person put it](https://www.linkedin.com/pulse/email-where-knowledge-goes-die-bill-french/):

> **email is where knowledge goes to die**

There is a better way, and that is to communicate about work with documentation
and issues in a tool like Github, Gitlab, or Gitea. If you want to propose a new
feature, or change how something is done, create a branch, modify some
documentation, and then open a pull request to discuss with others. This way any
outstanding discussions are captured in open pull requests rather than being
lost in the black hole of someone's inbox. Do we want to know what is
outstanding -- simple, look at outstanding issues and pull requests.

Old style maillists perhaps work around some of these issues with public list
archives, and have been successful in various OSS projects, but these style
lists are almost never used/accepted in company/business settings and are most
suitable for highly technical people working on very large OSS projects like the
Linux kernel. Smaller teams are better suited by a modern Git based workflow.

Email is very useful for discussions or to receive notifications from tools like
Github or Gitea (these emails can be deleted with no thought as we know the
information is captured elsewhere) and for communication that does not fit
anywhere else (ex: communication with people outside your organization). But if
there is a bit of information in an email that you want to save or make
available to others long term, it should be transferred somewhere else -- to a
shared documentation/collaboration system, or your own Notes system.

Some companies go as far as deleting all email older than 3 months old. This is
probably done to limit liability, but it is an excellent policy to help push
people in the right direction -- don't depend on email to store long lived
information.

Reference:

- [Email is Where Knowledge Goes to Die (Bill French)](https://www.linkedin.com/pulse/email-where-knowledge-goes-die-bill-french/)
- [Email is where knowledge goes to die (Rolf Lindström)](https://www.linkedin.com/pulse/email-where-knowledge-goes-die-rolf-lindstr%C3%B6m/)

## Instant Messaging

Instant messaging tools (like Signal, Google Hangouts, Slack, etc) can be useful
in collaboration in that they enable team members to strengthen relationships.
Signal is a good choice because it is simple to use, is end-to-end encrypted (so
they respect privacy), and there are both mobile and desktop clients. Slack may
be better for larger teams. Git notifications can be fed into a slack channel to
provide the team with a view into the work stream. Discourse not has chat
functionality making it a very powerful collaboration platform for teams. Chat
messages can be easily be turned into Forum topics to make them more accessible
long term.

## Meetings

Meetings are an interesting dynamic and perhaps one of the hardest things to get
right. At their worst, meetings are boring and a waste of time. At their best,
they can be invigorating and very enjoyable. The COVID experience has really
caused us to appreciate the value of the face to face experience. However, many
meetings (could be a formal business meeting or spending time with
friends/family) we still don’t really maximize the value of being in-person with
someone. In a meeting, everyone should be engaged during the entire meeting.
This means things need to keep moving. The focus needs to be on interpersonal
interaction, solving hard problems, brainstorming, making decisions, etc –
things that engage people. If one person does most of the taking, it is not a
meeting, it is a presentation.

The trend in meetings today is that most are looking at their computer or phones
most of the time vs being fully engaged with each other. This could mean several
things:

1. we are doing work during the meeting
2. people are bored and are not engaged
3. we have not prepared for the meeting

Perhaps this is a natural result of spending a lot of time on virtual meetings,
where we soon get used to working through meetings because we can get away with
it. However, this is a suboptimal use of time because we are trying to do two
things at once and it is likely we are not doing either one very well. If we are
not fully engaged in a meeting, do we really need to be there?

If we can remove the mundane from the meetings, then this opens up opportunity
for more engaging/creative discussions.
[Gitlab’s approach](https://changelog.com/podcast/397#transcript-86) is rather
profound:

> We actually place a very high burden on meetings, and we do it intentionally,
> so that meetings are not the default. And if you create a culture where
> meetings are hard to have and it’s not the default, then sure enough, the next
> path of least resistance will become the thing that people go to, which in our
> case is documentation.
>
> ...
>
> And then after the meeting you have to look at the takeaways in the Google
> Doc, which is just a temporal home, and you have to say “Alright, if anything
> in this meeting matters to more than just me, I have to go find the right
> place(s) in the handbook and make a merge request to actually add this to the
> handbook”, because the handbook is the ultimate single source of truth, where
> all of the company would go to to find the latest and greatest information on
> whatever the topic is. So what I’m saying is that’s a lot of work in a
> meeting…

Documentation should be the center of real work. If decisions and thinking are
not captured for easy access by others, it is of limited use and will likely
soon be lost. It is also important that thoughts be captured in a format that
can be easily modified and expanded on in the future. This is the power of
content stored in Git with pull requests – the thinking does not stop at the end
of the meeting, but rather the meeting can be the start of something that
continues on in one continuous flow of creative thought as people collaborate,
open pull requests, discuss them with comments, meet again to brainstorm and
solve hard problems, etc. A meeting is then not a disruptive speed bump in your
schedule, but rather a well synchronized input into the flow of creative work,
which all revolves around fluid documentation and never stops. Meetings are not
for status reports, boring monologues, power posturing, etc, but rather to
maximize the creative output of a group of people working together in real time.

For more ideas on the topic of meetings, see:

- `[Brain Drain: The Mere Presence of One’s Own Smartphone Reduces Available Cognitive Capacity](https://www.journals.uchicago.edu/doi/epdf/10.1086/691462)`
- [Gitlab's approach to meetings](https://changelog.com/podcast/397#transcript-86)
- [Seth Godin's ideas](https://seths.blog/2009/03/getting-serious-about-your-meeting-problem/)
- [Naval's appoach](https://nav.al/who-has-time-for-meetings)

## Kanban Boards

are useful tools for managing work and creating flexible and ad-hoc work-flows.
These boards can be implemented using post-it notes on a wall, or with digital
tools such as [Trello](https://trello.com). Typically a Kanboard board will have
_backlog_, _doing_, and _done_ lists to clearly the status of work. Items may be
sorted in the backlog according to priority.

Some points that make Kanban boards work well:

1. You can at a glance see what is going on.
2. All discussion happens in the card, so it is easy to find and refer to later.
3. Discussions have context (documents, photos, links, previous comments, etc).
   Everything is together rather than being in disconnected email threads,
   instant messaging comments, file server files, etc.
4. Workflow and processes can be managed by dragging cards from one list to
   another. Example sales -> orders -> part procurement -> production ->
   shipping -> done. These lists could be split into separate boards as needed.

Suggestions for making Kanban boards work well (the following applies to Trello,
but other tools may be similar):

1. Try to keep the number of lists to a number that they fit horizontally on a
   computer screen. If you need more lists than this, then create more boards.
   It is easy to move cards from one board to another.
1. If you have too many lists to fit on a screen, keep the most important ones,
   such as _doing_, to the left, so they show up when you load the page.
1. Use screenshots extensively. A picture is often worth a thousand words --
   paste screenshots into cards to convey status, results, problems, etc. This
   makes cards more interesting to read. It is often quicker to screenshot some
   code in an editor than try to do a text copy and get the result to look
   right.
1. Create a trello board that is your "library". This can contain lists of
   reference information, documentation, cards with datasheets, etc. The ability
   to add comments allows you to add meta information and makes the library more
   useful than just dumping a bunch of PDFs on a file server.

## Deep Work

Collaboration is essential, but you should consider allocating blocks of your
day where notifications are silenced so that you can focus on
[deep work](https://www.calnewport.com/books/deep-work/). Even if you only
allocate 3 hours a day to deep work, it amazing what you can get done in this
focused time if it is uninterrupted.
