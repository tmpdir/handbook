# The Development Process

**Contents**

<!-- toc -->

The development process has been extensively researched and there are many
processes or methodologies that have been proposed over time. This document
attempts to identify simple concepts that will be generally useful.

## Documentation

Documentation is an important part of the development process. See the
[Documentation chapter](documentation.md) for more information on this topic.

## The Development Triangle

Life is full of trade-offs – product/software development is no different. Fast,
Cheap, Features … pick two. Want all three? Then quality will suffer.

![development triangle](images/development-triangle.png)

## Why is Iteration important?

There are several attributes of modern systems that are perhaps different than
systems in the past:

- They are often complex such that no one person understands the entire system,
  or can even visualize all the possibilities. The most innovative and useful
  systems are a combination of many good ideas.
- There are plenty of resources for expansion (memory is cheap, processors are
  powerful, standard interfaces like USB allow for easy expansion)
- The system can be upgraded (software updates are standard)
- The problems they solve are complex and the problem often changes once people
  try something and better understand what can be done.
- Perfection comes from refining ideas, trying things, etc.

The software industry has long understood this and most development processes
have moved from a waterfall to agile type methods. However general project and
hardware development thinking tends to lag -- partly due to practical
considerations such as the cost of building physical prototypes. However, these
costs are becoming lower and lower each day and innovative companies have
embraced iteration. As
[this article](https://www.bradfordembedded.com/2012/03/iterate-hardware-like-software)
states so well:

> Apple does this with their hardware. Why don’t you?

## Business Relationship Models

The simple way of managing projects is the "throw it over the wall model." In
this model, you typically have a gatekeeper that manages the over-the-wall
transition.

![over the wall](images/wall.png)

This model works fairly well for simple systems or components that can be
clearly specified and no iterations are required. In this case, an experienced
gatekeeper may be able to add a lot of value in managing the process.
Gatekeepers may be a consultant, distributor, manager, purchasing agent, etc.
However, as the complexity of a system increases, the gatekeeper increasingly
finds they don't know everything about the system. Instead of improving the
process, they become a bottleneck.

As systems become more complex, the need to collaboration and project iteration
grows -- the success of open source software has long proven this. Thus having
processes where all parties involved can easily collaborate is critical.

![collaborative model](images/interate.png)

If open processes are not in place, the following happens:

- the best ideas are never discovered, as the right people never work together
- problems are extremely difficult to solve as the right people don't have
  visibility
- motivation suffers because not everyone gets to share in the project success
  or failure
- things move extremely slow

Thus, for modern systems, it is important to foster processes and environments
that encourage openness, collaboration, and iteration.

![Complexity/Collaboration Graph](images/complexity-collab-graph.png)
