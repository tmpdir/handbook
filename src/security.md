# Security

(also see [Operations](ops.md) and [Reliability](reliability.md))

With connected computers and devices, security is now an important
consideration. There are several aspects to consider:

## Threats

- privacy (I don't want others viewing my information)
- theft (I don't want others stealing my information)
- loss (I don't want others damaging or defacing my computers, equipment,
  information, web sites, etc)

### Privacy

Privacy in the Internet age is largely an illusion, especially if you use
mainstream cloud services. Even in you are on a perfectly secure computer
system, you still might send a private email to someone, and they might forward
it to people you did not intend to be recipients. The best policy with
communication is to write with the assumption the message may be posted on a
billboard some day. Never say anything about someone that you would not say to
their face. This is basic integrity.

### Theft

In the age of open source, your intellectual property is becoming less your
code, schematics, and product ideas, and more your ability to execute and scale.
The core technology we use (programming languages, browsers, operating systems,
etc.) are all projects that exceed the scale of even the largest companies and
governments. The hard part is integrating and managing this technology. Your
"process" is the new IP.

For information to have value for thieves, there has to be someone who is
interested in it. For most people and smaller companies, the only person
interested in your information is you. This is why ransomeware is becoming more
popular -- your information has very little value beyond yourself.

Another popular form of theft is when criminals compromise our computing
resources and use them for their own purposes. They may use them to launch DoS
attacks as a service, hide illegal activities, etc. This may even happen without
our knowledge. As high-speed, symmetrical fiber Internet connections become more
common, a simple Internet connected computer will become more and more valuable.

### Loss

We are concerned that we don't lose the use of our digital information,
infrastructure, or physical assets. Some examples:

- hackers maliciously destroy information
- hackers encrypt our data and demand ransom
- foreign agents compromise computers that control the power grid and shut off
  power or damage equipment
- we lose the password to our password database
- we lose the key to an encrypted hard drive and can't access our data
- security measures make our infrastructure hard to use, preventing work from
  getting done
- our password database is compromised, giving attackers access to every on-line
  account we operate.
- our computer is stolen, and ...
  - the thief uses saved passwords in the browser to compromise online accounts.
  - the thief uses ssh keys that are not password protected to log into servers
    we operate
- an attacker social engineers someone to give them the login information for
  various accounts
- our email server goes down, but a service required to run our email requires
  email 2FA to log in. Furthermore, this service only provides support through
  their web portal which we can't get to.
- someone launches a DNS attack on our service racking up huge hosting costs and
  [shuts down a large number](https://blog.tomilkieway.com/72k-1/) of online
  services we use
- the person who operates our infrastructure is out of commission and no one
  else has access
- a server crashes and we don't have backups or the backups don't work

The list goes on ... It because obvious there are many attack vectors that can
cause loss.

## Balance

With the topic of security, it is important to have some balance. If we are not
careful, security measures can cause more damage than the threat we are trying
to protect against. The benefit must always be weighed against the cost.

## SSH

SSH keys should always be password protected so that if a machine is stolen or
compromised, the keys can't be used to log into servers.

Password login should always be disabled on servers connected to the Internet
and SSH keys used instead. This is fairly easy to do and drastically reduces the
number of attacks. Even if you have very secure passwords, allowing password
login will cause attackers to unceasingly try dictionary attacks on the system.
More measure like fail2ban can be used, but it seems forcing key based login
gets you 99% of the way there.

## 2FA, MFA Authentication

Two factor (2FA) or Multi-factor (MFA) authentication is becoming popular and
now required for a number of services. Typically a email or SMS code is sent or
an authenticator app on a phone is required. While this can help prevent an
attacker from getting access to our account, it also increases the risk that we
can't access our account ourself. Consider the following scenarios:

- we lose the use of our email
  - our DNS domain is hijacked or expires
  - we lose our login information
  - payment method expires and the account is locked
- we lose the use of our phone
  - phone number is hijacked
  - phone is damaged, and we need our phone to log into the service that manages
    our phone service.

What is the impact of the above? How many days can we operate without these. Map
it and make sure you have a recovery plan.

It may make sense to assign multiple people to critical accounts. There is more
risk of the account being compromised, but less risk we'll be locked out of it.

Loosing access to our data through our own mistakes seems to be a much bigger
problem in general than malicious actors.
