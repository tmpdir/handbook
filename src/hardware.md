# Hardware

This document describes best practices and reference information useful when
designing hardware (printed circuit boards, etc).

## Tools

There is no one tool that is perfect for all jobs -- that idea is delusional.
The critical thing is to use the best tool for the job at hand. Some questions
to consider:

- cost of the tool. Is the price a purchase or subscription model.
- how often will I be using the tool. Am I an occasional user, or is this my
  full-time job?
- barrier to others contributing. Collaboration is key, so tool choice should be
  made with the thought of enabling collaboration.
- how long do I need to maintain the design? You will need access to tools for
  the life-cycle of the design.
- support -- is there a company or community who can support me when I have
  questions or problems?
- how am I going to maintain part numbers, BOMs and other manufacturing data?
- if outsourcing the design, will I have the ability to maintain it, or easily
  get someone else to work on it?

For basic designs KiCad works well. As of version 5.1, it has all the essential
features needed for efficiently capturing schematics and laying out PCBs.
However, if you have a large design with a lot of high speed signals, then you
may need something better.

This brings up the question -- why should I use KiCad when I have an Altium (or
some other high end tool) license? Lets phrase this another way -- even if you
owned an excavator, would you haul it in to dig a hole to plant a small tree in
your yard instead of owning a shovel? No, everyone with any property owns a
shovel, or likely several. It is quicker to get out for small tasks. Other
people (like your kids) can use it. Likewise, KiCad can be used on any computer
anywhere by anyone who spends a little time to learn it, instead of just at your
office on the machine that has the node-locked license. Someone else can make a
small tweak to the design, or easily take over maintenance. In 6 years, you
still have access to the tool if you need to make some changes because a part
went obsolete. With KiCad in your organization, more people will become PCB
designers. The manufacturing engineer may design a simple board to automate a
production task. A project manager may fix a minor issue in a released design
because no one else has time. For someone skilled in a high end tool, creating a
design in KiCad may take a little more time, but over the life-cycle of the
project, you will like save time.

Again, there are many factors and there is no one size fits all. Some designs
are already done and need to be maintained. Some designers are unwilling to use
something different than what they have used in the past. There are many
factors, but at least consider the right questions.

See this
[article](http://bec-systems.com/site/1670/reflections-on-kicad-and-eda-tools)
for more discussion on this topic.

## Prototyping

The cost of building PCB boards in low quantity is continually coming down, so
it is not much more practical to build prototypes often as advocated in
[this article](https://www.bradfordembedded.com/2012/03/iterate-hardware-like-software).
You learn a lot building something, so this should be done as early as practical
in the development process. Some places to order prototypes:

- [SEEED Fushion](https://www.seeedstudio.com/fusion_pcb.html)
- [Macrofab](https://macrofab.com/)
- [OSHPARK](https://oshpark.com/)

## Testing

TODO

## Schematic Checklist

1. decoupling caps located next to each other should be two decades apart,
   otherwise, they may form tank circuits and resonate.
1. any power loss should drive interrupts to the CPU, so that we can recover if
   necessary
1. review all external IRQs to make sure they are is no external IRQ muxing --
   especially on Linux systems, this can be a pain to implement.
1. create spreadsheet of IO and make sure all signals are available --
   especially on SOM designs
1. verify LCD data signal mapping is correct
1. verify signal levels between various parts
1. check pullups on all i2c signals -- ~5K
1. battery powered systems should have a low voltage shutoff -- otherwise
   batteries will be damaged
1. read battery voltage. Systems have a way of being used in applications that
   were not originally intended, and being able to read battery voltage is often
   helpful.
1. if battery and AC powered, a GPIO should be available to tell if AC power is
   available. Even better, be able to read the input voltage. This allows early
   detection of some power problems.
1. all power switches should be controlled slew rate. Use power switches instead
   of raw FETs.
1. power sourcing -- especially in battery powered designs
1. verify boot mode pins
1. power sequencing
1. place 0.1uF cap near the reset pin to help avoid ESD induced resets
1. use supervisory chip with threshold above brownout and connect to GPIO to
   warn SW of potential power problems
1. Verify trace widths of all high current traces
1. USB: (todo, locate source) Other VBUS considerations are the Inrush current
   which must be limited. This is outlined in the USB specification paragraph
   7.2.4.1 and is commonly overlooked. Inrush current is contributed to the
   amount of capacitance on your device between VBUS and ground. The spec
   therefore specifies that the maximum decoupling capacitance you can have on
   your device is 10uF. When you disconnect the device after current is flowing
   through the inductive USB cable, a large flyback voltage can occur on the
   open end of the cable. To prevent this, a 1uF minimum VBUS decoupling
   capacitance is specified.

## Schematic Style

1. Schematics are not only design entry — schematics are documentation. Unless
   you are writing all the software, handling all the manufacturing, and
   debugging all field issues yourself, there will be others who will be using
   them. Be considerate.
1. Not everyone has a D size plotter (even B size printers are fairly rare). I
   have a rather large (27″, 2560×1140) monitor, and have seen schematics that I
   have to zoom in to read. Schematics should be created such that they can be
   printed on letter size paper (everyone has a letter size printer), and easily
   read — even by folks in their 60’s. This is fairly simple — just spend some
   time breaking large symbols into smaller sections, and use a few more pages.
   Even for viewing on a screen, putting less stuff on each page makes it easier
   to scroll through pages. Most people print out schematics for review or bench
   work — so having letter size pages makes this possible.
1. Schematics should be organized to have one function per page, or group
   similar functions on a page. Don’t intermix power supplies and an audio codec
   unless they are related. We realize the importance of organizing software in
   modules, and a large schematic is really no different.
1. Keep all pages the same size. When every page is a different size, you can’t
   easily scroll through pages, or print them out.
1. Keep nesting levels of hierarchical schematics to a minimum. For most PCB
   designs, hierarchical schematics offer little advantage over a flat schematic
   logically organized on pages. PCB’s can only be so big, therefore even if you
   have 20 pages for a schematic, 20 pages is really not all that much, and
   adding hierarchy to 20 pages just adds complexity and makes them harder to
   read. In a sense, your schematic page becomes a “hierarchical block.” However
   in tools like KiCad, hiearchy is the only way to do multi-plage schematics
   and they can offer some benefit in showing how the different pages are
   connected together.
1. Off page references are the key to making smaller schematic pages work.
1. Spend a little more time putting a little art into your schematic symbols. If
   you have a DB-9 connector, and you draw a connector in the shape of a DB-9
   (only takes a few more minutes), then you can instantly recognize what it is
   in the schematic, and it is much quicker to find the circuit you are looking
   for during debugging sessions. The same can be said for USB connectors, etc.
   In some cases, it may make sense to draw a connector symbol to match how it
   looks physically, so you can quickly find a pin on the bench without digging
   out a datasheet.
1. Schematic text must be easily searchable. I don’t want to install a clunky
   schematic viewer that only runs on X.XX version of windows just to search for
   text. I want to use the PDF reader I already have installed. Make sure
   schematics are cleanly exported to PDF, and are easy to search.

Other references:

- [rules and guidelines for drawing good schematics](https://electronics.stackexchange.com/questions/28251/rules-and-guidelines-for-drawing-good-schematics)
