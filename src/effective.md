# Effective

Personally and organizationally, we desire to be effective and useful. Many of
the principles we find effective personally, are also effective
organizationally, but are often more difficult to apply organizationally.

## Attributes of Successful People and Organizations

- **Humility**. This has been well researched by Jim Collins in his book
  [Good to Great](https://www.jimcollins.com/books.html). There are few people
  who are humble, and even fewer organizations.
- **Truth**, the foundation of all success. Honesty is a component of this -- do
  we see things for what they are?
- **Improvement**. We are either moving forward or backward, there is no
  standing still. Those who "have arrived" are in decline. Are we continually
  learning? Do we leave things better than when we found them?
- **Collaboration**. On their own, humans can do very little. We are highly
  dependent on others. Smart people don't always know everything but often don't
  realize it. Effective people (and organizations) acknowledge what they don't
  know and connect with those who do.
- **Excellence**. My grandfather had a sign above his desk that said "Good
  enough is not good enough." Do we do the best possible job we can with the
  resources and time we have?
- **Generous**. What we give comes back to us many times over. Are we fair in
  our dealings?
- **Discipline**. Will and strength to focus on what needs done, and ignore
  other distractions.
- **Vision**. We have some idea where we are going. Is there meaning and
  purpose?
- **Trust**. The foundation of successful human collaboration. Nothing can
  replace this.
- **Care**. Do we care enough to understand others? Do we listen? Do we
  encourage others? Many succeed personally, but it is much harder
  organizationally.

We notice a trend here -- positive attributes are more difficult to find in
organizations than individuals. Organizations can be structured to leverage the
strengths of its individual members, or it can be reduced to the lowest common
denominator of its members. What is the difference?

## Attributes of Successful Teams and Projects

Below are some of the attributes of teams/project culture that contribute to
success:

- intrinsically motivated people (you have to start with this)
- very clear goals (with milestones)
- clear list what needed done and who is assigned to what specific task
- good communication
  - communication is [centered around documentation](documentation.md)
  - even though it is distracting at times, the best projects used instant/group
    messaging (skype, hangouts, signal, slack, etc) quite a bit. This seems
    important for relationships.
  - good use of issue tracking system (Trac, Github/Gitlab/Gitea issues, Trello,
    etc)
  - occasional face to face meeting (perhaps once or twice per year).
- developers get feedback on how what they are building is used.
- no politics.
- freedom to express thoughts and respectfully disagree with others without fear
  of repercussions
- no turf or rigid roles -- people did what needed done, and did not worry about
  stepping on someone else's toes
- openness - state of the project, how company was doing, field
  successes/failures was clearly communicated to all on the team
- everyone on the team had access to the same project resources, source code,
  etc.
- no barriers to communication -- you communicate with anyone on the project
  (even customers at times) and did not have to funnel information through a PM.
  However, because of the workflow (issue tracking system), a lot of
  communication (especially status and decisions) was also accessible to anyone
  on the team as needed.
- no blame culture. Mistakes will be made. Focus is on improving process,
  testing, etc. instead of blaming people when things go wrong. (Note, this does
  not excuse lazy or unmotivated people -- that is a different problem than
  making mistakes)
- low friction work-flow where experienced people review/approve work of less
  experienced people, but anyone is free to do anything needed.
- bias toward action, building, and early testing.

## Optimize where it will make a difference

A comment we sometimes hear when we ponder approaches is "Just keep it simple!"
The question we then must ask is "from whose perspective?" With any optimization
or trade-off, we need to always ask this question.

A few perspectives to consider:

- R&D
- time to market
- maintenance
- support
- end user
- sales
- manufacturing
- quality
- management
- financial
- business owner
- IT
- legal/IP
- security

It can be a little overwhelming when you consider how long the above list can
be. One approach is to consider who is spending the most time with the product.
Hopefully this is the user, otherwise the product is not successful. If you
don't optimize for the user's experience, then someone else will. The next
categories might be manufacturing/maintenance/support/sales -- at least for a
long lived product. Sustaining development effort far outweighs initial
development on most products. Additionally, sales, manufacturing, and support
efforts continue as long as the product exists. The time developers spend
working on a product swamp that of management and support functions like IT. We
obviously need to balance the concerns of everyone involved, but if we optimize
the tooling and flow for those who are spending the most time with the product,
that will probably pay the largest dividends.

Another approach to consider is try to implement tooling and processes that
benefit multiple perspectives. Some ideas to consider:

- select tooling that is open and can be customized as needed. Too often
  proprietary tooling is selecting from one perspective and it can't be easily
  adapted as new needs arise.
- ensure access to information is open. For example, if management has clear
  visibility and access to development workflow, then less meetings and manual
  reports will be required. If development has easy access to support and user
  information, they will do better job building something. The more easily
  information is shared and organized across organizational boundaries, the
  better things will go.
- automate - automate - automate. If something needs done more than a few times,
  automate it. This requires prudent technology selections that are amenable to
  automation and customization.
- remove unnecessary roadblocks and gatekeepers. If something needs done, make
  sure anyone with the skills can do it. Mistakes are costly, but not getting
  things done is likely more prevalent and more costly. The difference is the
  first is highly visible, and the latter can be hidden quite easily.
- focus on effective measurements that are visible to all. It's hard to improve
  without measurements.
