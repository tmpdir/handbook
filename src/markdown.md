# Markdown

There are many trade-offs with documentation formats. Many of these have been
discussed already. There are richer markup formats that are highly recommended
such as
[Asciidoc](https://www.ericholscher.com/blog/2016/mar/15/dont-use-markdown-for-technical-docs/)
and
[reStructuredText](https://buttondown.email/hillelwayne/archive/please-dont-write-your-documentation-in-markdown/).
If you are writing a book or public facing product documentation that needs
extensive formatting, then the more powerful markup languages are a good fit.
However, for development documentation where we're simply trying to capture some
knowledge, formatting is rarely large concern. The basics features of Markdown
are usually adequate and do not impose much burden on developers to learn.
Markdown is not the most powerful tool, but it is the pragmatic tool.

Additionally, many collaboration tools now use Markdown for comments,
descriptions. This is a much better experience than the WYSIWYG editors of yore.
Examples of tools that have embraced Markdown include Trello, Github, Gitlab,
Gitea, and Discourse. These tools are a joy to use. And one of the reasons is
the editing experience -- it is so simple and smooth, but yet Markdown gives you
the power of rich formatting when you need it.

The common use of Markdown gives you an easy way to migrate content from chat
messages to issues/forum comments to documentation.

## The CommonMark standard

The [CommonMark](https://commonmark.org/) specification that most tools now use
as a baseline for their Markdown implementations. It is called CommonMark
because the original Markdown creator
[did not allow the use](https://blog.codinghorror.com/standard-markdown-is-now-common-markdown/)
of the term Markdown.

## Formatting

Like code formatting, there is value in a team using the same format for
Markdown. Prettier does a good job at auto-formatting Markdown. Many editors can
be configured to format Markdown on save by adding the following file to your
project:

```
.prettierrc

printWidth: 80
proseWrap: always
```
