# Platform

**Contents**

<!-- toc -->

(also see [our podcast](https://tmpdir.org/014/) on this topic)

With complex systems, it is important to consider your technology platform which
is the base technology on which you your product is built. Some examples might
include:

- an operating system
- programming language
- tools
- System on Chip (SOC) and other complex integrated circuits
- application libraries/packages
- cloud providers (AWS, GCP, Azure, etc)
- knowledge re-use
- technology that we build and reuse over time

Modern products must be built on existing technology -- you don't have enough
time to re-invent everything from scratch. This reuse, which is greatest
leverage we can bring to modern products, is also our biggest risk, for it is
often problems in the base technology on which we build that is our undoing,
because these problems are out of our control. This has never been more evident
than in the chip shortages following the COVID-19 pandemic. Many companies are
having trouble shipping product due to some component shortage.

Everyone has their priorities, but common desirable qualities of our platform
might include:

- **available** - can we get the technology when we need it?
- **stable** - does it perform as we expect it to? can we get support or fix it
  when it breaks?
- **predictable** - does it always produce the same result?
- **adaptable** - can it be used in a wide range of products and problems?
- **composable** - can it be easily combined with other technologies?
- **reusable** - can we reuse our platform in multiple products?
- **extensible** - can new features be added easily?
- **openness** - is it easy to onboard new developers or is the platform locked
  up in the mind of of a few people? Is it well documented?
- **runway** - is your platform actively being developed to provide new features
  and take advantage of new technologies?
- **unique value** - does our platform enable us to build products that are
  unique and provide good value?
- **programmable** - can functionality be controlled in a deterministic and
  controlled manner (IE programs) and is it conducive to automation?

There are three perspectives to consider, which will be examined in turn.

1. selecting the right technology for your platform
1. having an appropriate amount of control over your platform
1. building and investing in your own platform

## Selecting the right technology

If we don't select the right technology to build on, not much else matters.
Using new technology brings significant advantages to a development, and also
risks. Companies who are in the mode doing things the way they always did them
whither and die. Companies that are reckless in adopting new technology often
crash and burn as they can never get things to work. The art is in knowing what
is available and when to adopt it.

A developer in this age must continually watch what is going on around him and
stay current with new developments. Time must be allocated for reading,
investigation, and experimentation. The superpower in this age is not
necessarily knowing everything, but knowing who to ask. Are you connected with
the people who are experts and can provide you with the information you need
when it comes time to select technology?

## Control

With your platform, control is also a balance. You can't realistically know and
do everything in your platform, but must have the ability to fix problems and
adapt the platform as needed. Are you connected with the right people and
communities who understand the technology you are using? Again, the superpower
is not in knowing everything, but knowing who to ask, and being humble enough to
admit when need help. Do you participate and add value in the open source
communities that produce the technologies you use, or do you just expect them to
fix your problems for free?

## Building

A successful platform must be to some extent unique. For small companies/teams,
it may be mostly a combination of other technologies/platforms and the value is
in how they are combined. But, there must be something unique about your
platform. If you build a product on an single existing platform without doing
anything hard, there is likely not much value in the product you are building.
For there to be value, you need to do hard stuff. And if you do hard stuff, it
is nice if you can easily re-use that effort in the future.

Successful technology platforms have values. These values attract developers
with similar values.

## Do you own your platform?

On the surface, this question may seem ridiculous, for no one company can own
all the technology that goes into modern products. However, to the extent you
own your platform will determine your destiny and success. Apple is an extreme
example of this -- they own their platform. Few of us will build things on that
scale, and many of us have no desire to work at that scale. But, there plenty of
hard problems left in this world that will only be solved by the magical
combination of reuse and innovation, and then being able to leverage this
development (your platform) is what produces high value.

## Your personal platform

Thus far, we've been discussing the concept of a platform in the context of a
company developing products, but we can apply these concepts personally as well.
What do you know? Who do you know? What experiences do you have? Have you
learned from them? What skills can you develop in the future? How can these be
combined to provide something unique and valuable? Are you doing hard stuff --
reusing what you have learned, or are you coasting? Are you generous with what
you know?

## Open Source

Many of the most useful and technologies available to developers today are now
open source. Some examples include:

- Linux
- Wordpress
- OpenEmbedded/Yocto
- languages like Go/Rust/Elm

## Examples

### Linux vs Windows CE

TODO

### Embedded Linux vs MCU/RTOS platforms

There have been so many MCU operating systems/platforms that have come and go
over the years. Years ago I recall being frustrated by the difficulty of
embedded Linux and was attracted to the Dallas Semi-conductor
[TINI platform](https://www.futurlec.com/News/Dallas/InternetIC.shtml). This was
a custom 8051 microcontroller designed to run Java code. TODO ...

### Yocto vs OpenWrt

### Cloud services vs your own server

## TODO

- media (own blog website vs Linkedin, twitter, etc)
- brand -- you are your own platform
-
