a product of:

[![BEC Logo](bec_logo.png)](http://bec-systems.com)

To contact:

- [http://bec-systems.com](http://bec-systems.com)
- [https://community.tmpdir.org](https://community.tmpdir.org)

Feedback is welcome! If you find this useful, let us know. If you
agree/disagree, let us know.

If you find this resource valuable, considering
[sponsoring](https://paypal.me/becsystems) this work.

# Overview

As we consider the efforts of humans, we see success and failure. We observe
projects that go well, and those that do not. Why is this? A complete answer is
beyond the scope of this document, and beyond the ability of any one person or
group of people to fully understand. There are many aspects of human life
including the physical, social, environmental, emotional, psychological, and
spiritual -- all powerfully affect and influence people. With our finite time
and understanding, we are going to focus on two aspects of human endeavor and
how they relate to success:

1. **scale**
2. **flow**

**Scale** is the ability to move an endeavor (project) beyond one person. A
single person can accomplish very little in this world. Unlike many of the
animals, we are helpless on our own. The progress that allows us to live in a
comfortable home, drive an automobile, and type on a computer are all the result
of countless human efforts -- each building on previous efforts. Even the most
notable of humans is a but a tiny blip in the chain of human progress. Yet most
people live in a delusional bubble that the world revolves around them. If what
we do cannot be used by others, we will likely have contributed very little to
the overall human condition beyond subsistence and our own personal wealth which
is like a vapor that appears for a moment, and then is gone. The effort and
discipline required to scale any effort beyond one person to even a few people
is immense. It requires a completely different mindset.

**Flow**, in the context of human endeavor, is when things proceed along
smoothly with continuous contributions and improvements -- like a smoothly
running flywheel that is started with great effort, and then gains momentum with
each incremental push and never stops. This can be contrasted with endeavors
that resembles dragging a large object up a hill. We jerk it along alone with
starts and stops until we are exhausted. We leave the object and beg and cajole
others to help us. With a group we make rapid progress for awhile, but then all
are exhausted and stop. Some leave. There is bickering and politics that some
are not pulling their load. We argue if pushing or pulling is better. If we are
not careful to brace the object, it may roll back down the hill at times. Which
analogy do your projects most resemble and why? The reality is likely somewhere
in the middle, but these extremes help us think about the big picture.

The TMPDIR handbook is a collection of observations and best practices. These
practices are designed to scale efforts beyond one person and achieve a
reasonable state of project flow. Although some practices may be specific to
areas the authors have worked in (hardware/software development), much should be
generally applicable.

Some specific goals might include:

- encourage and enable collaboration
- minimize mistakes
- reduce bureaucracy
- add process where beneficial
- connect you with the right people
- foster innovation

## Process

Process is a tricky thing to get right. Implemented poorly, it adds bureaucracy,
friction, and bottlenecks. Process implemented with a reactive mindset limits
what people can do and seeks to minimize damage due to a lack lack of integrity
and trust.

Process implemented properly decreases friction, automates tedious tasks, and
increases transparency. Because there is transparency, there is accountability
and trust. Process implemented with a progressive, forward looking mindset
enables people to do more. If something needs done, anyone on the team is free
to do it. But, because there is a process that records everything that is done,
there is also transparency and accountability.

## The Collaborative Mindset

The collaborative mindset works with the intention of enabling others and
communicating in an open and transparent fashion. Tools like Git allow all
communication (commits, tickets, pull requests) to happen in the open, but
people are free to ignore happenings that are not relevant to them. Instead of
design discussions happening in emails or meetings, a developer with an idea
might modify some documentation, create a pull request (PR) and tag others to
ask for review on the ideas. Discussion will follow in the pull request
comments. Another person who was not tagged in the PR might notice the PR in log
in the Git portal home page, decide it looks interesting, and offer a comment.
Other PRs can be easily referenced. We typically think of PRs happening in the
context of source code changes, but why can't they be just as effective for
documentation and design? However this is a big mindset change to shift away
from emails and meetings that we are accustomed to using.

## Scarcity vs Abundance Mindset

There are two approaches in business -- one is to view opportunities as a fixed
pie and try to get the largest slice possible. The other approach is to grow the
pie so there is more opportunity for everyone and the slice you have gets larger
as well.

## Gatekeeper vs Enabler Mindset

Related to the scarcity vs abundance mindset is the distinction between our role
in projects and companies. Are we a gatekeeper, or an enabler? A gatekeeper
tightly controls who can do his job, thus tends to do things in a closed way.
Enablers do things in an open way that enables others to easily contribute. The
enabler mindset is not a free-for-all as there can still be reviews, checks, and
quality standards. But, there are no barriers to work shifting to whoever has
time and interest.
